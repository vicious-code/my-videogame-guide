import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { RecentRoutingModule } from "./recent-routing.module";
import { RecentComponent } from "./recent.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        RecentRoutingModule
    ],
    declarations: [
        RecentComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class RecentModule { }
