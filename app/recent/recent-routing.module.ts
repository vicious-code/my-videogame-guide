import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { RecentComponent } from "./recent.component";

const routes: Routes = [{ path: "", component: RecentComponent }];

@NgModule({
  imports: [
    NativeScriptRouterModule.forChild(routes),
    NativeScriptUIListViewModule
  ],
  exports: [NativeScriptRouterModule, NativeScriptUIListViewModule]
})
export class RecentRoutingModule {}
