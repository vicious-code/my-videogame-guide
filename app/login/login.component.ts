import { Component, ElementRef, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { alert } from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page"; 
import app from "~/shared/app/app";
import { User } from "../shared/user/user.model";
import { UserService } from "../shared/user/user.service";
import { ApiService } from "~/shared/services/api.services";
import { BackendService } from "~/shared/services/backend.service";
import 'rxjs/add/operator/map';

@Component({
  selector: "login",
  providers: [UserService, ApiService],
  templateUrl: "login/login.component.html", 
  styleUrls: ["login/login.component.css"]
})
export class LoginComponent {
  isLoggingIn = true;
  user: User;
  app: any;
  loading:boolean = false;
  enabled: boolean = true;
  @ViewChild("password") password: ElementRef;
  @ViewChild("confirmPassword") confirmPassword: ElementRef;

  constructor(
    private page: Page,
    private userService: UserService,
    private router: Router,
    private apiService: ApiService
  ) {
    this.page.actionBarHidden = true;
    this.user = new User();
    this.app = app;
    // console.log(this.user, "user");
    this.user.email = "l.velasquez1308@gmail.com";
    this.user.password = "testing";
  }

  toggleForm() {
    console.log(this.isLoggingIn);
    this.isLoggingIn = !this.isLoggingIn;
  }

  submit() {
    this.enabled = false;
    if (!this.user.email || !this.user.password) {
      this.alert("Please provide both an email address and password.");
      return;
    }

    if (this.isLoggingIn) {
      this.login();
    } else {
      this.register();
    }
  }

  login() {
    this.loading = true;
    this.userService.login(this.user).subscribe((res: any) => {
      this.enabled = true;
      this.loading = false;
      BackendService.token = res.access_token;
      this.apiService.getUser().subscribe((res: any) => {
        BackendService.username = res.name;
        BackendService.email = res.email;
        this.router.navigate(["/home"]);
      })
    });
    // this.userService
    //   .login(this.user)
    //   .then(res => {
    //     this.enabled = true;
    //     this.router.navigate(["/home"]);
    //   })
    //   .catch(() => {
    //     this.enabled = true;
    //     this.alert("Unfortunately we could not find your account.");
    //   });
  }

  register() {
    // if (this.user.password != this.user.confirmPassword) {
    //   this.alert("Your passwords do not match.");
    //   return;
    // }
    this.userService
      .register(this.user)
      .subscribe(() => {
        this.enabled = true;
        this.alert("Your account was successfully created.");
        this.isLoggingIn = true;
      });
      // .catch(() => {
      //   this.enabled = true;
      //   this.alert("Unfortunately we were unable to create your account.");
      // });
  }
  // TODO: finish forgot password
  // forgotPassword() {
  //     prompt({
  //         title: "Forgot Password",
  //         message: "Enter the email address you used to register for APP NAME to reset your password.",
  //         inputType: "email",
  //         defaultText: "",
  //         okButtonText: "Ok",
  //         cancelButtonText: "Cancel"
  //     }).then((data) => {
  //         if (data.result) {
  //             this.userService.resetPassword(data.text.trim())
  //                 .then(() => {
  //                     this.alert("Your password was successfully reset. Please check your email for instructions on choosing a new password.");
  //                 }).catch(() => {
  //                     this.alert("Unfortunately, an error occurred resetting your password.");
  //                 });
  //         }
  //     });
  // }

  focusPassword() {
    this.password.nativeElement.focus();
  }
  focusConfirmPassword() {
    if (!this.isLoggingIn) {
      this.confirmPassword.nativeElement.focus();
    }
  }

  alert(message: string) {
    return alert({
      title: this.app.name,
      okButtonText: "OK",
      message: message
    });
  }
}
