import { Component, OnInit, ViewChild } from "@angular/core";
import * as app from "application";
import { RouterExtensions } from "nativescript-angular/router";
import {
  DrawerTransitionBase,
  RadSideDrawer,
  SlideInOnTopTransition
} from "nativescript-ui-sidedrawer";
import firebase = require("nativescript-plugin-firebase");
import { BackendService } from "~/shared/services/backend.service";
import { User } from "~/shared/user/user.model";
import { UserService } from "~/shared/user/user.service";

@Component({
  selector: "ns-app",
  templateUrl: "app.component.html",
  providers: [UserService]
})
export class AppComponent implements OnInit {
  private _selectedPage: string;
  private _sideDrawerTransition: DrawerTransitionBase;
  user: User;

  constructor(
    private routerExtensions: RouterExtensions,
    private userService: UserService
  ) {
    this.user = new User();
    // Use the component constructor to inject services.
  }

  ngOnInit(): void {
    if(BackendService.isLoggedIn) {
      this.user.name = BackendService.username;
      this.user.email = BackendService.email;
    }
    // Init firebase
    // firebase
    //   .init({
    //     // storageBucket: "gs://nintendo-sw-store.appspot.com",
    //     persist: false,
    //     // Optionally pass in properties for database, authentication and cloud messaging,
    //     // see their respective docs.
    //     onAuthStateChanged: data => {
    //       if (data.loggedIn) {
    //         BackendService.token = data.user.uid;
    //       } else {
    //         BackendService.token = "";
    //       }
    //       console.log(data);
    //       this.user.name = data.user.name;
    //       this.user.email = data.user.email;
    //       // optional but useful to immediately re-logon the user when he re-visits your app
    //       console.log(
    //         data.loggedIn ? "Logged in to firebase" : "Logged out from firebase"
    //       );
    //       // if (data.loggedIn) {
    //       //   console.log(data.user.uid);
    //       //   console.log(
    //       //     "user's email address: " +
    //       //       (data.user.email ? data.user.email : "N/A")
    //       //   );
    //       // }
    //     }
    //   })
    //   .then(
    //     instance => {
    //       console.log("firebase.init done");
    //     },
    //     error => {
    //       console.log(`firebase.init error: ${error}`);
    //     }
    //   );
    
    this._selectedPage = "/home";
    this._sideDrawerTransition = new SlideInOnTopTransition();
  }

  get sideDrawerTransition(): DrawerTransitionBase {
    return this._sideDrawerTransition;
  }

  isPageSelected(pageTitle: string): boolean {
    return pageTitle === this._selectedPage;
  }

  onNavItemTap(navItemRoute: string): void {
    this._selectedPage = navItemRoute;
    this.routerExtensions.navigate([navItemRoute], {
      transition: {
        name: "fade"
      }
    });

    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.closeDrawer();
    if (navItemRoute == "/login") {
      sideDrawer.gesturesEnabled = false;
    } else {
      sideDrawer.gesturesEnabled = true;
    }
  }

  logout(): void {
    this.userService.logout();
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.gesturesEnabled = false;
    sideDrawer.closeDrawer();
  }

  onLoaded() {
    // checking if gesture should be on in the sidedrawer
    firebase
      .getCurrentUser()
      .then(user => {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.gesturesEnabled = true;
      })
      .catch(error => {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.gesturesEnabled = false;
      });
  }
}
