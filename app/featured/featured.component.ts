import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { UserService } from "~/shared/user/user.service";
import { ApiService } from "~/shared/services/api.services";
import { LoadingIndicator } from "nativescript-loading-indicator";
import { BackendService } from "~/shared/services/backend.service";
import * as _ from "lodash";
import { SearchBar } from "ui/search-bar";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
  selector: "Featured",
  providers: [UserService, ApiService],
  moduleId: module.id,
  templateUrl: "../shared/views/game.component.html",
  styleUrls: ["./featured.component.css"]
})
export class FeaturedComponent implements OnInit {
  allGames: any = [];
  games: any = [];
  links: any;
  itemPagesLoaded: number = 1;
  searchPhrase: string;
  loadMore: boolean = true;
  private indicator: LoadingIndicator;
  screenTitle: string;

  constructor(
    private userService: UserService,
    private apiService: ApiService,
    private routerExtensions: RouterExtensions
  ) {}

  ngOnInit(): void {
    this.screenTitle = "Favourites Games";
    this.getGames();
  }
  /**
   * Loading indicator
   * @param msj message to show in indicator
   */
  getIndicator(msj: string): LoadingIndicator {
    let indicator = new LoadingIndicator();

    return indicator.show({
      message: msj,
      progress: 0.65,
      android: {
        indeterminate: true,
        cancelable: true,
        max: 100,
        progressNumberFormat: "%1d/%2d",
        progressPercentFormat: 0.53,
        progressStyle: 2,
        secondaryProgress: 1
      }
    });
  }
  /**
   * Load all games
   */
  getGames() {
    let indicator = this.getIndicator("Loading Games...");
    this.apiService.getFavouritesGames().subscribe((res: any) => {
      this.allGames = res.data;
      this.games = res.data;
      this.links = res.links;
      indicator.hide();
    });
  }
  /**
   * Load more games for infinite scroll
   */
  loadMoreGames() {
    if (!this.loadMore || this.links.next == null) {
      return;
    }
    let indicator = this.getIndicator("Loading More Games...");
    // Load more items here.
    this.apiService.getMoreGames(this.links.next).subscribe((res: any) => {
      _.forEach(res.data, (v, k) => {
        this.allGames.push(v);
      });
      this.games = this.allGames;
      this.links = res.links;
      indicator.hide();
    });
  }
  /**
   * We have to know which picture to show
   * @param fav is favourite or not
   */
  getFavourite(fav: boolean) {
    return fav ? "res://add_to_fav_1" : "res://add_to_fav";
  }
  /**
   * Toogling favourite game status
   * @param game
   */
  toogleFavourite(game) {
    this.apiService
      .toggleFavourite(game.id, game.favourite)
      .subscribe((res: any) => {
        game.favourite = !game.favourite;
      });
  }
  /**
   * Formatting sale price
   */
  getPrice(price: number): string {
    if (!isNaN(price)) {
      return `$ ${price.toFixed(2)}`;
    } else {
      return "";
    }
  }

  getPrices(game) {
    let regularPrice = `Regular Price: $ ${game.eshop_price} \n`;
    let salePrice =
      game.sale_price > 0 ? `Sale Price: $ ${game.sale_price}\n` : "";
    let salesEnd =
      game.sale_price > 0 ? `Sale Price: $ ${game.sales_end}\n` : "";

    return regularPrice + salePrice;
  }
  /**
   * Handling click event in list view
   * @param args clicked element in list view
   */
  onItemTap(args) {
    const game = this.games[args.index];
    this.routerExtensions.navigate(["/game-detail", game.id]);
  }

  onSearchLayoutLoaded(event) {
    if (event.object.android) {
      event.object.android.setFocusableInTouchMode(true);
    }
  }
  /**
   * Remove focus in the searchbar when view load first time
   * @param event
   */
  onSearchBarLoaded(event) {
    if (event.object.android) {
      event.object.android.clearFocus();
    }
  }
  /**
   * Search bar submit
   * @param args searchbar result
   */
  onSubmit(args) {
    let searchBar = <SearchBar>args.object;
    alert("You are searching for " + searchBar.text);
  }
  /**
   * Search bar listener
   * @param args search bar result
   */
  onTextChanged(args) {
    this.loadMore = false;
    let searchBar = <SearchBar>args.object;
    if (typeof searchBar.text === "undefined") {
      return;
    }
    let searchValue = searchBar.text.toLowerCase();
    this.games = [];
    if (searchValue !== "" && this.allGames.length > 0) {
      for (let i = 0; i < this.allGames.length; i++) {
        if (this.allGames[i].title.toLowerCase().indexOf(searchValue) !== -1) {
          this.games.push(this.allGames[i]);
        }
      }
    } else {
      this.loadMore = true;
      this.games = this.allGames;
    }
  }

  templateSelector = (item: any, index: number, items: any) => {
    return item.type || "cell";
  };
  /**
   * Formating eshop price
   * @param val price
   */
  price(val: number) {
    if (val == 0) {
      return "N/A";
    } else {
      return "$ " + val;
    }
  }
  /**
   * Handling side drawer tap
   */
  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }
  /**
   * Closing session
   */
  logout(): void {
    this.userService.logout();
  }
  /**
   * When the view load we have to active gesture if user is logged
   */
  onLoaded(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    if (BackendService.token !== "") {
      sideDrawer.gesturesEnabled = true;
    } else {
      sideDrawer.gesturesEnabled = false;
    }
  }
}
