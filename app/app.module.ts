import {
  NgModule,
  NgModuleFactoryLoader,
  NO_ERRORS_SCHEMA
} from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "~/login/login.component";
import { AuthGuard } from "~/shared/services/auth.service";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { TNSFrescoModule } from "nativescript-fresco/angular";

import * as frescoModule from "nativescript-fresco";
import * as applicationModule from "tns-core-modules/application";
import { GameComponent } from "~/game/game.component";

if (applicationModule.android) {
  applicationModule.on("launch", () => {
    frescoModule.initialize();
  });
}

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    AppRoutingModule,
    NativeScriptModule,
    NativeScriptUISideDrawerModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
    TNSFrescoModule
  ],
  providers: [AuthGuard],
  declarations: [AppComponent, LoginComponent,
    GameComponent
],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
