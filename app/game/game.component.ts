import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ScrollEventData, ScrollView } from "ui/scroll-view";
import { ApiService } from "~/shared/services/api.services";
import { PageRoute, RouterExtensions } from "nativescript-angular/router";
import "rxjs/add/operator/switchMap";
import { Game } from "~/shared/models/game.model";
import { IGDBService } from "~/shared/services/igdb.service";

@Component({
  selector: "app-game",
  templateUrl: "./game/game.component.html",
  styleUrls: ["./game/game.component.css"],
  providers: [ApiService, IGDBService]
})
export class GameComponent implements OnInit { 
  game: Game;
  id: string;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private pageRoute: PageRoute,
    private igdbService: IGDBService,
    private routerExtensions: RouterExtensions,
  ) {
    this.game = new Game();
    // this.game.cover = new Cover();
    this.pageRoute.activatedRoute
      .switchMap(activatedRoute => activatedRoute.params)
      .forEach(params => {
        this.id = params["id"];
      });
  }

  ngOnInit() {
    this.apiService.getGame(this.id).subscribe((res: any) => {
      this.game = res.data;
      this.game.description = this.game.description.replace(/\s+/g, " ").trim();
    });

    // this.igdbService.getGame(this.name).subscribe((res: Game) => {
    //   this.game = res[0];
    //   // convirtiendo arreglo de generos a string
    //   this.genresStr = "";
    //   // this.game.genres.forEach(genre => {
    //   //   this.genresStr += genre.name + ', ';
    //   // });
    //   // this.genresStr = this.genresStr.replace(/,\s*$/, '');
    //   // convertir fecha de lanzamiento a humano
    //   var utcSeconds = this.game.first_release_date;
    //   var d = new Date(utcSeconds); // The 0 there is the key, which sets the date to the epoch
    //   this.date = d.toDateString();
    // });
  }

  showGenres(genres:Array<any>) {
    if(typeof genres == 'undefined') { return }
    let finalRes = '';

    genres.forEach((e) => {
      finalRes += ' ' + e + ',';
    })

    return finalRes;
  }

  public goBack() {
    this.routerExtensions.back();
  }

  // getRating(): string {
  //   if (typeof this.game.total_rating === "undefined") {
  //     return "N/A";
  //   } else {
  //     return this.game.total_rating == 0
  //       ? "N/A"
  //       : "Rating " + this.game.total_rating.toFixed(2);
  //   }
  // }

  // getSrc(): string {
  //   if (typeof this.game.cover === "undefined") {
  //     return "";
  //   } else {
  //     return (
  //       "https://images.igdb.com/igdb/image/upload/t_screenshot_med/" +
  //       this.game.cover.cloudinary_id
  //     );
  //   }
  // }
}
