import { NgModule } from "@angular/core";
import { Routes, CanActivate } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { LoginComponent } from "~/login/login.component";
import { AuthGuard } from "./shared/services/auth.service";
import { GameComponent } from "~/game/game.component";

const routes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  // { path: "", redirectTo: "/home", pathMatch: "full" },
  {
    path: "home",
    loadChildren: "./home/home.module#HomeModule",
    canActivate: [AuthGuard]
  },
  {
    path: "recent",
    loadChildren: "./recent/recent.module#RecentModule",
    canActivate: [AuthGuard]
  },
  {
    path: "future",
    loadChildren: "./future/future.module#FutureModule",
    canActivate: [AuthGuard]
  },
  {
    path: "browse",
    loadChildren: "./browse/browse.module#BrowseModule",
    canActivate: [AuthGuard]
  },
  {
    path: "search",
    loadChildren: "./search/search.module#SearchModule",
    canActivate: [AuthGuard]
  },
  {
    path: "featured",
    loadChildren: "./featured/featured.module#FeaturedModule",
    canActivate: [AuthGuard]
  },
  {
    path: "settings",
    loadChildren: "./settings/settings.module#SettingsModule",
    canActivate: [AuthGuard]
  },
  {
    path: "game-detail/:id",
    component: GameComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
