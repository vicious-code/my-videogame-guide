import { Injectable } from "@angular/core";
import { User } from "~/shared/user/user.model";
import { Observable as RxObservable } from "rxjs";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { RouterExtensions } from "nativescript-angular/router";
import { BackendService } from "~/shared/services/backend.service";
import api from "~/shared/api/api";

@Injectable()
export class UserService {
  constructor(private routerExtensions: RouterExtensions, private http: HttpClient) {}

  // register(user: User) {
  //   return new Promise((resolve, reject) => {
  //     firebase
  //       .createUser({
  //         email: user.email,
  //         password: user.password
  //       })
  //       .then(resolve)
  //       .catch(error => console.log(error));
  //   });
  // }

  register(user: User) {
    return this.http.post(api.url + `register`, {
      name: user.name,
      email: user.email,
      password: user.password
    })
  }

  login(user: User) {
    let headers = this.createRequestHeader();
    return this.http.post(api.tokenUrl + `token`, {
      // headers: headers,
      grant_type: "password",
      client_id: 2,
      client_secret: api.userKey,
      username: user.email,
      password: user.password
    }, { headers});
  }
  

  logout(): void {
    BackendService.token = "";
    BackendService.username = "";
    BackendService.username = ""; 
    this.routerExtensions.navigate(["/login"]);
  }

  private createRequestHeader() {
    // set headers here e.g.
    let headers = new HttpHeaders({
      // "user-key": api.userKey,
      Accept: "application/json",
      Authorization: "Bearer " + BackendService.token
    });

    return headers;
  }
}
