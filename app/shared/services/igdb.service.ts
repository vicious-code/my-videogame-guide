import { Injectable } from "@angular/core";
import { Observable as RxObservable } from "rxjs";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { BackendService } from "~/shared/services/backend.service";
import * as _ from "lodash";

@Injectable()
export class IGDBService {
  url:string = "https://api-endpoint.igdb.com/";
  nswId:number = 130;

  constructor(private http: HttpClient) {}

  getGame(name:string) { 
    name = this.toTitleCase(name);
    name = encodeURIComponent(name);
    let headers = this.createRequestHeader();
    // let filter = `games/?filter[platforms][eq]=${this.nswId}&fields=name,summary,rating,popularity,total_rating,developers,publishers,first_release_date,cover,genres.name&filter[name][eq]=${name}`;
    // let filter = `games/?filter[platforms][eq]=${this.nswId}&fields=name,summary,rating,popularity,total_rating,developers,publishers,first_release_date,cover,genres.name&search=${name}`;
    let filter = `games/?fields=name,summary,rating,popularity,total_rating,developers,publishers,first_release_date,cover,genres,name&search=${name}`;
    return this.http.get(this.url + filter, { headers});
  }

  private createRequestHeader() {
    // set headers here e.g.
    let headers = new HttpHeaders({
      // "user-key": api.userKey,
      Accept: "application/json",
      "user-key": "3d0f4fb441a99364ba80acd52e74ef91"
    });

    return headers;
  }

  private toTitleCase(str) {
    return _.startCase(_.toLower(str));
  }
}
