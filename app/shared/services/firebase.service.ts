import { Injectable } from "@angular/core";
import { Observable as RxObservable } from "rxjs";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import api from "~/shared/api/api";
import firebaseWebApi = require("nativescript-plugin-firebase/app");

@Injectable()
export class FirebaseService {
  constructor(private http: HttpClient) {}

  getGames() {
    firebaseWebApi
      .database()
      .ref("/games")
      .once("value")
      .then(result => console.log(JSON.stringify(result)))
      .catch(error => console.log("Error: " + error));
  }

  getResponseInfo() {
    let headers = this.createRequestHeader();
    return this.http.get(api.url, { headers: headers });
  }

  private createRequestHeader() {
    // set headers here e.g.
    let headers = new HttpHeaders({
      "user-key": api.userKey,
      Accept: "application/json"
    });

    return headers;
  }
}
