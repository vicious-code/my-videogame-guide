import { Injectable } from "@angular/core";
import { getString, setString } from "application-settings";

const tokenKey = "token";

export class BackendService {
  static isLoggedIn(): boolean {
    return !!getString("token");
  }

  static get token(): string {
    return getString("token");
  }

  static set token(theToken: string) {
    setString("token", theToken);
  }

  static get username(): string {
    return getString("username");
  }

  static set username(theToken: string) {
    setString("username", theToken);
  }
  static get email(): string {
    return getString("email");
  }

  static set email(theToken: string) {
    setString("email", theToken);
  }

}
