import { Injectable } from "@angular/core";
import { Observable as RxObservable } from "rxjs";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import api from "~/shared/api/api";
import { BackendService } from "~/shared/services/backend.service";

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {}

  login() {
    let headers = this.createRequestHeader();
    return this.http.post(api.tokenUrl + `token`, {
      // headers: headers,
      grant_type: "password",
      client_id: 2,
      client_secret: api.userKey,
      username: "l.velasquez1308@gmail.com",
      password: "testing"
    }, { headers});
  }
  getUser() {
    let headers = this.createRequestHeader();
    return this.http.get(api.url + `user`, { headers});
  }
  
  getGames() {
    let headers = this.createRequestHeader();
    return this.http.get(api.url + `games`, {
      headers: headers
    });
  }

  getRecentGames() {
    let headers = this.createRequestHeader();
    return this.http.get(api.url + `recentGames`, {
      headers: headers
    });
  }

  getFavouritesGames() {
    let headers = this.createRequestHeader();
    return this.http.get(api.url + `favourites`, {
      headers: headers
    });
  }

  getMoreGames(url) {
    let headers = this.createRequestHeader();
    return this.http.get(url, { headers});
  }

  getGame(id) {
    let headers = this.createRequestHeader();
    return this.http.get(api.url + `games/${id}`, { headers })
  }

  toggleFavourite(id:number, state:boolean) {
    let headers = this.createRequestHeader();
    if(state) {
      // delete
      return this.http.delete(api.url + `favourite-delete?game_id=${id}`, {headers})
    } else {
      // create
      return this.http.post(api.url + `favourite-create`, {game_id: id}, {headers})
    }
    
  }

  getResponseInfo() {
    let headers = this.createRequestHeader();
    return this.http.get(api.url, { headers: headers });
  }

  private createRequestHeader() {
    // set headers here e.g.
    let headers = new HttpHeaders({
      // "user-key": api.userKey,
      Accept: "application/json",
      Authorization: "Bearer " + BackendService.token
    });

    return headers;
  }
}
