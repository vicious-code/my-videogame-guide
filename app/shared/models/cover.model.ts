export class Cover {
  public cloudinary_id: string;
  public url: string;
  public height: number;
  public width: number;

}
