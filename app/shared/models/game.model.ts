import { Cover } from "~/shared/models/cover.model";

export class Game {
  public id: string;
  public title: string;
  public image: string;
  public number_of_players: string;
  public release_date: string;
  public system: string;
  public publisher: string;
  public developer: string;
  public description: string;
  public eshop_price: number;
  public sale_price: number;

  // public name: string;
  // public summary: string;
  // public rating: number;
  // public popularity: string;
  // public total_rating: number;
  // public developers: Array<any>;
  // public publishers: Array<any>;
  // public first_release_date: number;
  // public cover: Cover;
  // public genres: Array<any>;


}
