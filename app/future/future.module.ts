import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { FutureRoutingModule } from "./future-routing.module";
import { FutureComponent } from "./future.component";

@NgModule({
  imports: [NativeScriptCommonModule, FutureRoutingModule],
  declarations: [FutureComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class FutureModule {}
